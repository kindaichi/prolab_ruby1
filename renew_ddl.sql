drop table if exists users;
create table users (
      user_id       varchar(50)  not null primary key,
      name          varchar(50)  not null,
      age           int          not null);

