

drop table if exists datas;

create table datas (
  id          char      not null,
  kotoba      text      not null,
  story       text      not null,
  fi_kaito    text      not null,
  se_kaito    text      not null,
  fi_kaito_no char      not null,
  se_kaito_no char      not null);
