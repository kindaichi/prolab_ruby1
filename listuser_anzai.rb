# -*- coding: utf-8 -*-
require 'rubygems'
require 'dbi'

# これはデータベースのresultsテーブルからデータを読み込んで表示するプログラム
class ListUser
  def initialize( db_name )
    @db_name = db_name
    @dbh = DBI.connect( "DBI:SQLite3:#{@db_name}" )
  end

  # テーブル上の項目名を日本語に変えるハッシュテーブル
  def outputUsersList
    item_name = { 'user_id' => "ユーザーID",
                  'name' => "ユーザー名", 'age' => "年齢" }

    puts "  ゲームの結果表示"
    print "ゲームの結果を表示します。"

    puts "\n------------------------"

    # テーブルからデータを読み込んで表示する
    # user_id = x で入力してもらうかどここかから値を取ってくると表示できる
    # select文でデータベースを読み込み、whereでどこの箇所を読み込むか条件を指定
    sth = @dbh.execute("select * from users")
#      where user_id = '9'")

    sth.each { |row|
      row.each_with_name { |val, name|
        puts "#{item_name[name]}: #{val.to_s}"
      }
     puts "-------------------------"
     
    }
    sth.finish

  end

end
